import Foundation

public class Device{
    var xPointer:Int=0
    var yPointer:Int=0
    var yScroll:Int=0
    public var position:String {
        return "Position{ X:\(xPointer) Y:\(yPointer) Y-Scroll:\(yScroll)}"
    }
}
