import Foundation
public class Mouse{
    public var device:Device = Device()
    public init (device: Device){
        self.device = device
    }
    public func movePointerUp(){
        device.yPointer+=10
    }
    public func movePointerDown(){
        device.yPointer-=10
    }
    public func movePointerLeft(){
        device.xPointer-=10
    }
    public func movePointerRight(){
        device.xPointer+=10
    }
    public func scrollDown(){
        device.yScroll-=10
    }
    public func scrollUp(){
        device.yScroll+=10
    }
}
